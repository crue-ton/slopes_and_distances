# sd_calc
*simple calculator to get the distance and slopes of lines*

## Configuring
* Edit the **config.def.h** file, there will be a template set out for you
* Make sure to remove **config.h** before compiling to use changes

## Compiling
* Check out the **config.mk** file to change CFLAGS
* You can also just comment out CFLAGS if you don't want any
* Make sure to change the CFLAGS though

Once you have finished with the **config.mk** file, simply run:
```bash
make
```

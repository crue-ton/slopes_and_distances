typedef struct {
  int x, y;
} Vec2;

Vec2 points[] = {
  { .x = -9, .y = 8 },
  { .x = -8, .y = 1 },
  { .x = -3, .y = -4 },
  { .x = -4, .y = 3 }
};

char lines[4][3] = {
  "IJ",
  "JK",
  "KL",
  "LI"
};

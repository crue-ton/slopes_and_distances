#include <math.h>
#include <stdio.h>

#include "config.h"

// Returns in radical form
int get_length(Vec2 p1, Vec2 p2) {
  int result = pow((p1.x - p2.x), 2) + pow((p1.y - p2.y), 2); 

  return result;
}

Vec2 get_slope(Vec2 p1, Vec2 p2) {
  Vec2 result;

  result.y = p1.y - p2.y;
  result.x = p1.x - p2.x;

  return result;
}

int main() {
  size_t n = sizeof(points) / sizeof(points[0]);
  int lengths[n];
  Vec2 slopes[n];

  for(int i = 0; i < n; i++) {
    lengths[i] = get_length(points[i], i < 3 ? points[i+1] : points[0]);
    slopes[i] =  get_slope( points[i], i < 3 ? points[i+1] : points[0]);
  }

  for(int i = 0; i < n; i++)
    printf("\n%s\n\tPoint: (%d, %d)\n\tSlope: %d/%d\n\tDistance: √%d\n", lines[i], 
                                                                         points[i].x,
                                                                         points[i].y,
                                                                         slopes[i].y, 
                                                                         slopes[i].x, 
                                                                         lengths[i]);
  return 0;
}
